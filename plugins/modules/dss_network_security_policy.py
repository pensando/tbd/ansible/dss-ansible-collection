#!/usr/bin/python3

DOCUMENTATION = '''
---
module: dss_network_security_policy
short_description: Module to configure the PSM NetworkSecurityPolicy object on PSM for DSS
description: This module is used to configure the PSM NetworkSecurityPolicy object
options:
      attach-tenant:
        description: specifies if the set of rules need to be attached globally to a tenant.
        type: boolean
      rules:
        description: list of rules of type securitySGRule.
        type: array
'''

EXAMPLES = """
---
- hosts: localhost
  connection: local
  gather_facts: no

  vars:
    psm_ip: 35.232.24.156:9876
    psm_username: '{{ lookup("env", "PSM_USER") }}'
    psm_passwd: '{{ lookup("env", "PSM_PASSWORD") }}'

  tasks:

    - name: Test Network Security Policy
      dss_network_security_policy:
        state: present
        policy_name: test_policy_new
        tenant: default
        psm_user: '{{ psm_username }}'
        psm_password: '{{ psm_passwd }}'
        psm_host: '{{ psm_ip }}'
        attach_tenant: true
        rules:
          - proto_ports:
              - protocol: udp
                ports: '111-222'
            action: permit
            from_ip_addresses: [ 1.1.1.1, 2.2.2.2, 3.3.3.3 ]
            to_ip_addresses: [ 5.5.5.5, 6.6.6.6, 7.7.7.7, 8.8.8.8 ]
          - proto_ports:
              - protocol: tcp
                ports: '22'
            action: permit
            from_ip_addresses:
              - 10.10.10.10
            to_ip_addresses:
              - 20.20.20.20
          - proto_ports:
              - protocol: udp
                ports: '169'
              - protocol: tcp
                ports: '80-92,1010-1100'
              - protocol: tcp
                ports: '9000,9100,9200'
            action: deny
            from_ip_addresses:
              - 12.12.12.12
              - 13.13.13.13
              - 14.14.14.14
            to_ip_addresses:
              - 42.24.42.24
"""

RETURN = '''
obj:
    description: NetworkSecurityPolicy (api/network) object
    returned: success, changed
    type: dict
'''


# Load Ansible mod utils
from ansible.module_utils.basic import AnsibleModule

import warnings

# Import Pensando SDK
import pensando_dss
import pensando_dss.psm
from pensando_dss.psm.api import security_v1_api
from pensando_dss.psm.model.security_network_security_policy import *
from pensando_dss.psm.model.api_status import ApiStatus
from pensando_dss.psm.model.api_object_meta import *
from pensando_dss.psm.models.security import *

def policy_exists(api_instance, name, tenant):
   try:
        api_response = api_instance.get_network_security_policy1(name)
   except pensando_dss.psm.ApiException as e:
        return False
   return api_response

def main():

  warnings.simplefilter("ignore")

  argument_specs = dict(
        state=dict(default='present',
                   choices=['absent', 'present']),
        tenant=dict(type='str', default='default'),
        policy_name=dict(type='str', required=True),
        psm_user=dict(type='str', required=False),
        psm_password=dict(type='str', required=False),
        psm_basic_auth=dict(type='bool', required=False, default=False),
        psm_config_path=dict(type='str', required=False),
        psm_host=dict(type='str', required=True),
        rules=dict(type='list', required=False),
        attach_tenant=dict(type='bool', required=True)
  )

  module = AnsibleModule(
      argument_spec=argument_specs, supports_check_mode=False)

  state = module.params['state']
  tenant = module.params['tenant']
  policy_name = module.params['policy_name']
  psm_user = module.params['psm_user']
  psm_password = module.params['psm_password']
  psm_host = module.params['psm_host']
  psm_basic_auth=module.params['psm_basic_auth']
  psm_config_path=module.params['psm_config_path']
  if 'rules' in module.params:
       rules = module.params['rules']
  attach_tenant = module.params['attach_tenant']

  try:
    configuration = pensando_dss.psm.Configuration(
      psm_basic_auth = psm_basic_auth,
      psm_config_path = psm_config_path,
      host = psm_host,
      username = psm_user,
      password = psm_password
    )
    configuration.verify_ssl = False
  except Exception as e:
      module.fail_json (msg="Failed PSM Login", exception=e)
      module.exit_json(changed=False)


  with pensando_dss.psm.ApiClient(configuration) as api_client:
      # Create an instance of the API class
      api_instance = security_v1_api.SecurityV1Api(api_client)

      # example passing only required values which don't have defaults set
      if state == "present":
           if policy_exists(api_instance, policy_name, tenant):
                module.fail_json (msg=f"Failed to create existing network security policy: {policy_name} ")
                module.exit_json(changed=False)
           meta=ApiObjectMeta(name=policy_name)

           newrules = []
           for rule in rules:
                pports = []
                for pp in rule['proto_ports']:
                    pport = SecurityProtoPort(ports=pp['ports'],protocol=pp['protocol'])
                    pports.append(pport)
                nrule = SecuritySGRule(action=rule['action'], from_ip_addresses=rule['from_ip_addresses'], 
                                                to_ip_addresses=rule['to_ip_addresses'], proto_ports=pports)
                newrules.append(nrule)

           spec = SecurityNetworkSecurityPolicySpec(attach_tenant=attach_tenant, rules=newrules) 
           body = SecurityNetworkSecurityPolicy(meta=meta, spec=spec)

           try:
               # Create NetworkSecurityPolicy object
               api_response = api_instance.add_network_security_policy1(body)
               module.exit_json(changed=True)
           except pensando_dss.psm.ApiException as e:
               module.fail_json (msg="Failed API add_network_security_policy", exception=e)
               module.exit_json(changed=False)
      elif state == "absent":
           try:
             # Delete NetworkSecurityPolicy object
             # api_response = api_instance.delete_network_security_policy1(tenant, policy_name)
             api_response = api_instance.delete_network_security_policy1(policy_name)
             module.exit_json(changed=True)
           except pensando_dss.psm.ApiException as e:
             module.fail_json (msg="Failed API delete_network_security_policy", exception=e)
             module.exit_json(changed=False)
 
if __name__ == '__main__':
    main()
