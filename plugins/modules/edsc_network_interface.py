#!/usr/bin/python3

DOCUMENTATION = '''
---
module: edsc_network_interface
short_description: Module to configure PF's to VLANs on PSM for Enterprise DSCs
description:  This module associates a PF network interface to an existing VLAN.  Ensure that this is supported on a given PSM distribution.
options:
       state:
            description: Must be 'present'.   'absent' not supported.
            type: string
       interface_name:
            description:  Name of PF
            type:  string
       attach_network:
            description:  VLAN name with which to associate the PF
            type:  string
       mac_address:
            description: MAC address of the PF
            type: string
       tenant:
            description: Must be 'default'
            type: string

'''

EXAMPLES = """
- hosts: localhost
  connection: local
  gather_facts: no

  vars:
    psm_ip: 34.66.2.196:9876
    psm_username: '{{ lookup("env", "PSM_USER") }}'
    psm_passwd: '{{ lookup("env", "PSM_PASSWORD") }}'

  tasks:

    - name: Update Network Interface
      edsc_network_interface:
        state: present
        tenant: default
        psm_user: '{{ psm_username }}'
        psm_password: '{{ psm_passwd }}'
        psm_host: '{{ psm_ip }}'
        interface_name: aaaa.bbbb.cc01-pf-69
        attach_network: VLAN-101
        mac_address: aaaa.bbbb.cc01
"""

RETURN = '''
obj:
    description: Network Interface object
    returned: success, changed
    type: dict
'''


# Load Ansible mod utils
from ansible.module_utils.basic import AnsibleModule
from ansible.module_utils.common.validation import check_type_dict
import json
import jsonpickle
import warnings
import time
import os
import pensando_ent
import pensando_ent.psm
from pensando_ent.psm.api import network_v1_api
from pensando_ent.psm.models.network import *
from pensando_ent.psm.model.network_network_interface import NetworkNetworkInterface
from pensando_ent.psm.model.api_status import ApiStatus
from dateutil.parser import parse as dateutil_parser

def main():
    warnings.simplefilter("ignore")
    argument_specs = dict(
        state=dict(default='present',
                   choices=['absent', 'present']),
        tenant=dict(type='str', default='default'),
        psm_user=dict(type='str', required=True),
        psm_password=dict(type='str', required=True),
        psm_host=dict(type='str', required=True),
        interface_name=dict(type='str', required=True),
        attach_network=dict(type='str', required=True),
        mac_address=dict(type='str', required=True)
    )

    module = AnsibleModule(
      argument_spec=argument_specs, supports_check_mode=False)

    state = module.params['state']
    tenant = module.params['tenant']
    psm_user = module.params['psm_user']
    psm_password = module.params['psm_password']
    psm_host = module.params['psm_host']
    interface_name = module.params['interface_name']
    attach_network = module.params['attach_network']
    mac_address = module.params['mac_address']

    try:
        configuration = pensando_ent.psm.Configuration(
                 psm_basic_auth = True,
                 host = psm_host,
                 username = psm_user,
                 password = psm_password,
        )
        configuration.verify_ssl = False
    except e:
        module.fail_json (msg="Failed PSM Login", exception=e)
        module.exit_json(changed=False)


    # Enter a context with an instance of the API client
    with pensando_ent.psm.ApiClient(configuration) as api_client:
        # Create an instance of the API class
        api_instance = network_v1_api.NetworkV1Api(api_client)
        try:
            body = NetworkNetworkInterface(
                meta=ApiObjectMeta(
                   name=interface_name
                ),
                spec=NetworkNetworkInterfaceSpec(
                    attach_network=attach_network,
                    attach_tenant=tenant,
                    mac_address=mac_address,
                    type="host-pf"
                )
            ) # NetworkNetworkInterface |
        except ApiError as e:
           module.fail_json (msg="Failed API Call", exception=e)
           module.exit_json(changed=False)


        api_response = {}
        # example passing only required values which don't have defaults set
        if state == 'present':
            try:
                # Update NetworkInterface object
                api_response = api_instance.update_network_interface(interface_name, body)
                r = jsonpickle.encode(api_response)
                module.exit_json(changed=True, response=r)
                module.exit_json(changed=True)
            except pensando_ent.psm.ApiException as e:
               module.fail_json (msg="Failed API add_network", exception=e)
               module.exit_json(changed=False, api_response=api_response)
        elif state == "absent":
               module.fail_json (msg="absent not supported")
               module.exit_json(changed=False, api_response=api_response)

if __name__ == '__main__':
    main()
