#!/usr/bin/python3

DOCUMENTATION = '''
---
module: dss_network_ipcollection_v2
short_description:  Module to configure the PSM NetworkIPcollection object on PSM for DSS
description: This module is used to configure the PSM NetworkIPcollection object
options:
    dss_network_ipcollection:
        description:  Name of IPcollection
        type: string
        tenant: default

'''


EXAMPLES = """

- hosts: localhost
  connection: local
  gather_facts: no

  vars:
    psm_ip: '{{ lookup("env", "PSM_IP") }}'
    psm_username: '{{ lookup("env", "PSM_USER") }}'
    psm_passwd: '{{ lookup("env", "PSM_PASSWORD") }}'

  tasks:

    - name: Create/Delete Network IPcollections
      dss_network_ipcollection:
        state: present
        psm_user: '{{ psm_username }}'
        psm_password: '{{ psm_passwd }}'
        psm_host: '{{ psm_ip }}'
        object:
         - meta:
              name: 192.168.42.1-10
           spec:
              addresses : 
               - 192.168.42.1-192.168.42.10
         - meta:
              name: 192.168.42.11-20
           spec:
              addresses : 
               - 192.168.42.11-192.168.42.20

"""

RETURN = '''
obj:
    description: Network IPcollection (api/network) object
    returned: success, changed
    type: dict
'''


# Load Ansible mod utils
from ansible.module_utils.basic import AnsibleModule
from ansible.module_utils.common.validation import check_type_dict
import json
import time
import os
import warnings
import requests
import pensando_dss
import pensando_dss.psm
from pensando_dss.psm.api import network_v1_api
from pensando_dss.psm.models.network import *
from pensando_dss.psm.model.api_status import ApiStatus
from pensando_dss.psm.exceptions import UnauthorizedException
from pensando_dss.psm.model.network_ip_collection import NetworkIPCollection

from dateutil.parser import parse as dateutil_parser

def network_ipcollection_exists(api_instance, name):
   try:
        api_response = api_instance.get_ip_collection1(name)
   except pensando_dss.psm.ApiException as e:
        return False
   return api_response

def network_ipcollection_equal(api_instance, name, addresses):
   try:
        api_response = api_instance.get_ip_collection1(name)
        if api_response.spec.addresses:
             if len(api_response.spec.addresses) != len(addresses):
                 return False
             for i in range(len(api_response.spec.addresses)):
                 if api_response.spec.addresses[i] == addresses[i]:
                       continue
                 else:
                       return False
             return True
        return False
   except pensando_dss.psm.ApiException as e:
        return False

def get_ipcollection_body(m, s):
    body = NetworkIPCollection(
        meta=ApiObjectMeta( name=m['name']), 
        spec=NetworkIPCollectionSpec( addresses=s['addresses'])
    )
    return body


def main():

    warnings.simplefilter("ignore")
    argument_specs = dict(
        state=dict(default='present',
                   choices=['absent', 'present']),
        tenant=dict(type='str', default='default'),
        psm_user=dict(type='str', required=False),
        psm_password=dict(type='str', required=False),
        psm_host=dict(type='str', required=False),
        psm_basic_auth=dict(type='bool', required=False, default=False),
        psm_config_path=dict(type='str', required=False),
        allow_dups=dict(type='bool', required=False, default=False),
        allow_missing=dict(type='bool', required=False, default=False),
        allow_dup_proto_ports=dict(type='bool', required=False, default=False),
        object=dict(type='list', required=True),
    )

    module = AnsibleModule(
      argument_spec=argument_specs, supports_check_mode=False)

    state = module.params['state']
    tenant = module.params['tenant']
    psm_host = module.params['psm_host']
    psm_user = module.params['psm_user']
    psm_password = module.params['psm_password']
    psm_basic_auth=module.params['psm_basic_auth']
    psm_config_path=module.params['psm_config_path']
    allow_dups = module.params['allow_dups'] 
    allow_missing = module.params['allow_missing']
    object = module.params['object']

    # See configuration.py for a list of all supported configuration parameters.
    try:
        configuration = pensando_dss.psm.Configuration(
              psm_basic_auth = psm_basic_auth,
              psm_config_path = psm_config_path,
              host = psm_host,
              username = psm_user,
              password = psm_password
        )
        configuration.verify_ssl = False
    except (pensando_dss.psm.exceptions.UnauthorizedException, requests.exceptions.HTTPError)  as e:
        module.fail_json (msg=f"Failed PSM Login.  HTTP Error:  {e.response.status_code} ")
        module.exit_json(changed=False)



    # Enter a context with an instance of the API client
    with pensando_dss.psm.ApiClient(configuration) as api_client:
        # Create an instance of the API class
        api_instance = network_v1_api.NetworkV1Api(api_client)
        o_tenant = "default"

        api_response = {}
        # example passing only required values which don't have defaults set
        if state == 'present':
           count = 0
           for obj in object:
                try:
                    ipc = network_ipcollection_exists (api_instance, obj['meta']['name'])
                    if ipc:
                        if allow_dups:
                            if network_ipcollection_equal (api_instance, obj['meta']['name'], obj['spec']['addresses']):
                                 module.log (f"Skipping duplicate: {obj['meta']['name']}")
                                 continue
                            else:
                                 module.fail_json (msg=f"NetworkIPCollection: {obj['meta']['name']} already exists with different definition")
                                 module.exit_json(changed=False)
                        else:
                            module.fail_json (msg=f"NetworkIPCollection: {obj['meta']['name']} already exists")
                            module.exit_json(changed=False)
                    # Create IPCollection object
                    body = get_ipcollection_body (obj['meta'], obj['spec'])
                    api_response = api_instance.add_ip_collection1(body)
                    count = count + 1
                except pensando_dss.psm.ApiException as e:
                    module.fail_json (msg="Failed API add_ip_collection1", exception=e)
                    module.exit_json(changed=False)
           module.exit_json(changed=count)
        elif state == 'absent':
           deleted = 0
           for obj in object:
                try:
                    ipc = network_ipcollection_exists (api_instance, obj['meta']['name'])
                    if not ipc:
                        if allow_missing:
                            continue
                        else:
                            module.fail_json (msg=f"IPCollection: {obj['meta']['name']} does not exist")
                            module.exit_json(changed=False)
                    # Delete IPCollections object
                    api_response = api_instance.delete_ip_collection1(obj['meta']['name'])
                    deleted = deleted + 1
                except pensando_dss.psm.ApiException as e:
                    module.fail_json (msg="Failed API delete_ipcollection", exception=e)
                    module.exit_json(changed=False)
           module.exit_json(changed=deleted)

if __name__ == '__main__':
    main()
