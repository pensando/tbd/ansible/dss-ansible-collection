#!/usr/bin/python3



import warnings

# Import Pensando SDK
import pensando_dss
import pensando_dss.psm
from pensando_dss.psm.api import security_v1_api
from pensando_dss.psm.model.security_network_security_policy import *
from pensando_dss.psm.model.api_status import ApiStatus
from pensando_dss.psm.model.api_object_meta import *
from pensando_dss.psm.models.security import *
from pensando_dss.psm.exceptions import *
  
def policy_exists(api_instance, name, tenant):
   try:
        api_response = api_instance.get_network_security_policy1(name)
   except pensando_dss.psm.ApiException as e:
        return False
   return api_response

#
# Are two arrays equal?
#
def arrayEqual(arr1, arr2, n, m):
    # If lengths of array are not
    # equal means array are not equal
    if (n != m):
        return False

    # Sort both arrays
    arr1.sort()
    arr2.sort()

    # Linearly compare elements
    for i in range(0, n):
        if (arr1[i] != arr2[i]):
            return False

    # If all elements were same.
    return True

#
# Test if two rules are equal
#
def rules_equal (r1, r2):
   if r1['action'] != r2['action']:
         return False
   if 'from_ip_addresses' in r1:
       if len(r1['from_ip_addresses']) != len(r2['from_ip_addresses']):
            return False
       if not arrayEqual (r1['from_ip_addresses'], r2['from_ip_addresses'], len(r1['from_ip_addresses']), len(r2['from_ip_addresses'])):
            return False
   if 'from_ipcollections' in r1:
       if len(r1['from_ipcollections']) != len(r2['from_ipcollections']):
            return False
       if not arrayEqual (r1['from_ipcollections'], r2['from_ipcollections'], len(r1['from_ipcollections']), len(r2['from_ipcollections'])):
            return False
   if 'to_ip_addresses' in r1:
       if len(r1['to_ip_addresses']) != len(r2['to_ip_addresses']):
            return False
       if not arrayEqual (r1['to_ip_addresses'], r2['to_ip_addresses'], len(r1['to_ip_addresses']), len(r2['to_ip_addresses'])):
            return False
   if 'to_ipcollections' in r1:
       if len(r1['to_ipcollections']) != len(r2['to_ipcollections']):
            return False
       if not arrayEqual (r1['to_ipcollections'], r2['to_ipcollections'], len(r1['to_ipcollections']), len(r2['to_ipcollections'])):
            return False
   if 'apps' in r1:
       if len(r1['apps']) != len(r2['apps']):
            return False
       if not arrayEqual (r1['apps'], r2['apps'], len(r1['apps']), len(r2['apps'])):
            return False

   # Old and deprecated
   # if len(r1['proto_ports']) != len(r2['proto_ports']):
   #      return False
   # proto_ports now restricted to one per rule ...
   # if r1['proto_ports'][0]['ports'] != r2['proto_ports'][0]['ports']:
   #       return False
   # if r1['proto_ports'][0]['protocol'] != r2['proto_ports'][0]['protocol']:
   #      return False

   return True

def rule_exists(rule, rulelist):
   for r in rulelist:
      if rules_equal(rule, r):
          return True
   return False


def get_idx(rulelist, rule):
   idx = 0
   for r in rulelist:
        if rules_equal (r, rule):
           return idx
        idx = idx + 1
   raise RuntimeError ("rule (", rule, ") not found")


#
# Update 'rules' from an existing policy
#
def policy_update_rules(policy, rules, op, attach_tenant):
    meta = ApiObjectMeta(name=policy.meta.name)
    newrules = policy.spec.rules
    for r in rules:
       if op == "delete":
          idx = get_idx (policy.spec.rules, r)
          del newrules[idx]
       else:
         if "update" in r:                 # update in place
              idx = get_idx(policy.spec.rules, r)
              for k in r['update']:
                   newrules[idx][k] = r['update'][k]
         elif "row" in r:                # insert operation
              idx = r['row'] - 1
              del r['row']                 # delete the 'row' kv pair
              newrules.insert(idx, r)
         else:
              newrules.append(r)
    nrules = []
    for rule in newrules:
        pports = []
        for pp in rule['proto_ports']:
              pport = SecurityProtoPort(ports=pp['ports'],protocol=pp['protocol'])
              pports.append(pport)
        nrule = SecuritySGRule(action=rule['action'], from_ip_addresses=rule['from_ip_addresses'],
                               to_ip_addresses=rule['to_ip_addresses'], proto_ports=pports)
        nrules.append(nrule)

    spec = SecurityNetworkSecurityPolicySpec(attach_tenant=attach_tenant, rules=nrules)
    newpolicy = SecurityNetworkSecurityPolicy(meta=meta, spec=spec)
    return newpolicy


